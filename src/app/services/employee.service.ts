import {Injectable} from '@angular/core';
import {Employee} from '../shared/employee';

import {baseURL} from '../shared/baseurl';


import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/delay';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import {HttpClient} from "@angular/common/http";

@Injectable()
export class EmployeeService {

  constructor(private http: HttpClient) {
  }

  getEmployees(): Observable<Employee[]> {
    return this.http.get(baseURL + 'employees')
      .map((res: Employee[]) => {
        return res;
      });
  }

  getEmployee(id: number): Observable<Employee> {
    return this.http.get<Employee>(baseURL + 'employees/' + id)
      .map(res => {
        return res;
      });
  }

  getFeaturedEmployee(): Observable<Employee> {
    return this.http.get<Employee>(baseURL + 'employees?featured=true')
      .map(res => {
        return res[0];
      });
  }

  getEmployeeIds(): Observable<number[]> {
    return this.getEmployees()
      .map(employees => {
        return employees.map(employee => employee.id)
      });
  }
}
