import {Component, Inject, OnInit} from '@angular/core';
import {Employee} from '../shared/employee';
import {EmployeeService} from '../services/employee.service';

import {ActivatedRoute, Params} from '@angular/router';
import {Location} from '@angular/common';
import 'rxjs/add/operator/switchMap';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {

  employee: Employee;
  employeeIds: number[];
  prev: number;
  next: number;

  constructor(private employeeservice: EmployeeService,
              private route: ActivatedRoute,
              private location: Location,
              @Inject('BaseURL') private BaseURL) {
  }

  ngOnInit() {
    this.employeeservice.getEmployeeIds().subscribe(employeeIds => this.employeeIds = employeeIds);
    this.route.params
      .switchMap((params: Params) => this.employeeservice.getEmployee(params['id']))
      .subscribe(employee => {
        this.employee = employee;
        this.setPrevNext(employee.id);
      });
  }
  goBack(): void {
    this.location.back();
  }

  setPrevNext(employeeId: number) {
    let index = this.employeeIds.indexOf(employeeId);
    this.prev = this.employeeIds[(this.employeeIds.length + index - 1) % this.employeeIds.length];
    this.next = this.employeeIds[(this.employeeIds.length + index + 1) % this.employeeIds.length];
  }
}
