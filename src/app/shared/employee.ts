export class Employee {
    id: number;
    firstName: string;
    lastName: string;
    name: string;
    image: string;
    jobPosition: string;
    jobCode: string;
    featured: boolean;
    jobDescription: string;
}
