import {Component, Inject, OnInit} from '@angular/core';
import {Employee} from "../shared/employee";
import {EmployeeService} from "../services/employee.service";

@Component({
    selector: 'app-about',
    templateUrl: './about.component.html',
    styleUrls: ['./about.component.scss']
})
export class AboutComponent implements OnInit {

    private employees: Employee[];
    private selectedEmployee: Employee;

    constructor(private employeeService: EmployeeService, @Inject('BaseURL') private BaseURL) {
    }

    ngOnInit() {
        this.employeeService.getEmployees()
            .subscribe(employees => this.employees = employees);
    }

    onViewProfile(employee: Employee){
      this.selectedEmployee = employee;
    }

}
